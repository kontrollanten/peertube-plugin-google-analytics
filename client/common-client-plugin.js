function register ({ registerHook, peertubeHelpers }) {
  initGa(registerHook, peertubeHelpers)
    .catch(err => console.error('Cannot initialize Google Analytics', err))
}

export {
  register
}

function initGa (registerHook, peertubeHelpers) {
  return peertubeHelpers.getSettings()
    .then(settings => {
      const trackingCode = settings['ga-tracking-code']

      if (!trackingCode) {
        console.error('Google Analytics settings are not set.')
        return
      }

      const trackPlayerAction = ({
        action,
        name,
      }) =>
        gtag('event', action, {
          event_category: 'video',
          event_label: name,
        })

      window.dataLayer = window.dataLayer || []
      window.gtag = function() {
        window.dataLayer.push(arguments);
      };

      registerHook({
        target: 'action:video-watch.player.loaded',
        handler: ({ player, video, videojs }) => {
          trackPlayerAction({
            action: 'video_load',
            name: video.name,
          })

          player.on('play', p => {
            trackPlayerAction({
              action: 'video_play',
              name: video.name,
            })
          })

          player.on('pause', p => {
            trackPlayerAction({
              action: 'video_pause',
              name: video.name,
            })
          })

          player.on('playing', p => {
            trackPlayerAction({
              action: 'video_playing',
              name: video.name,
            })
          })
        },
      })

      gtag('js', new Date())

      const script = document.createElement('script')
      script.type = 'text/javascript'
      script.async = true
      script.src = `https://www.googletagmanager.com/gtag/js?id=${trackingCode}`
      document.body.appendChild(script)

      gtag('config', trackingCode, {
        allow_google_signals: false,
      })

      registerHook({
        target: 'action:router.navigation-end',
        handler: () => gtag('config', trackingCode),
      })
    })

}

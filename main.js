async function register ({
  registerHook,
  registerSetting,
  settingsManager,
  storageManager,
  videoCategoryManager,
  videoLicenceManager,
  videoLanguageManager
}) {
  registerSetting({
    name: 'ga-tracking-code',
    label: 'Google Analytics measurement ID',
    type: 'input',
    private: false,
  })
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
